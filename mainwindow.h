#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
struct wordbook
{
    int size;
    QString **BookPtr;
};
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void WordBookRead(char*,wordbook &);
private:
    Ui::MainWindow *ui;
    wordbook en_ru,en_de,de_ru;


 public slots:
     void Translating();

 signals:
     void Translated(QString);
};

#endif // MAINWINDOW_H
