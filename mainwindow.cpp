#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <iostream>
#include <fstream>
#include <string>
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
//#include <ctype.h>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    MainWindow::WordBookRead("d:/eng-rus.txt",en_ru);
    MainWindow::WordBookRead("d:/eng-deu.txt",en_de);
    MainWindow::WordBookRead("d:/deu-rus.txt",de_ru);
    QObject::connect(ui->pushButton,SIGNAL(clicked()),
                     this ,
                     SLOT(Translating()));
}

MainWindow::~MainWindow()
{
    delete ui;
    if (en_de.BookPtr) delete[] en_de.BookPtr;
    if (en_ru.BookPtr) delete[] en_ru.BookPtr;
    if (de_ru.BookPtr) delete[] de_ru.BookPtr;
}

void MainWindow::WordBookRead(char* file,wordbook &curr)
{
   std::ifstream WB(file);

   if (!WB)
       {
        emit ui->label->setText("wordbook read error!");
       }
   else
       {

    std::string temp;
    int numStrings = 0;

    while ( std::getline( WB, temp ) )
        {
            numStrings++;
        };
    curr.size=numStrings;
    //QString** bookptr
     curr.BookPtr = new QString* [curr.size];
    //BookPtr=bookptr;


    for (int i = 0; i < curr.size; i++)
        {
            curr.BookPtr[i] = new QString [2];
        };

    QString ttemp;
    char buff[256];
    WB.close();
    WB.open(file);

    for (int j = 0; j < curr.size; j++)
        {
           WB.getline(buff,255);
           ttemp=buff;
           for (int x = 0; x < 2; x++)
            {
            curr.BookPtr[j][x]=ttemp.section(',',x,x);
            };
        };

    WB.close();
       };
}


void MainWindow::Translating()

{  emit ui->label->setText("start translate");

    QString tText =ui->lineEdit->text();

    QStringList list = tText.split((QRegExp("\\b")));
    wordbook current;
    short int curr1=ui->comboBox->currentIndex();
    short int curr2=ui->comboBox_2->currentIndex();
    int lang=(curr1*10+curr2);
    bool _i,_j;
    switch (lang)
    {
    case 01 :case 10:
        current=en_de;
        break;
    case 02 :case 20:
        current=en_ru;
        break;
    case 12:case 21:
        current=de_ru;
        break;
    default:
        break;
    }
    div_t d=div(lang,10);
    if ((d.quot)>(d.rem))
    {
        _i=1;
        _j=0;
    }
    else
    {
        _i=0;
        _j=1;
    }
    for (int j=0; j<current.size;j++)
    {
        for (int i=0; i<list.size();i++)
        {
            if (list.at(i)==current.BookPtr[j][_i])
            {
            list[i]=current.BookPtr[j][_j];
            };

        };
    };
    emit ui->lineEdit_2->setText( list.join(' ') );
}
